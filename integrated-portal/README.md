# Apigee Integrated Developer Portal custom theming

This folder contains the following files:

* `custom-styles.scss`
* `favicon.ico`
* `README.md` (the file you're reading)
* `variables.scss`
* `/assets`
* `/pages`
* `/menus`
* `/apis`

[Please keep this list, and this README file up to date.]

## SCSS Files

The two .scss files in this folder contain the SCSS code to style the Integrated Developer Portal provided by Apigee's Saas Management system.

* _variables.scss_: contains variable overrides and new variables.
* _custom-styles.scss_: contains the actual styles that change the look and feel of the Portal. Requires the _variables.scss_ file.

### How to use the files

* Log into Apigee as the administrator.
* Click on the **Portals** icon on the main page, or click **Publish > Portals** in the sidebar menu.
* Click on the name of the Portal you want to theme. Usually there is only one.
* Click on the **Theme** icon.
* In the top right corner, click on **Advanced**.
* Copy and paste the contents of the _variables.scss_ file into the **Variables** section.
* Copy and paste the contents of the _custom-styles.scss_ file into the **Custom Styles** section. _(Now you see why the files are named that way, right??)_
* Click the **Save** button that appeared in the top right corner.
* When the button disappears, you can close the Advanced Theme Editor window.
* On this page you can upload your logo.
* Click the **Logo options** link to expose the fields to upload the mobile logo and favicon.
* The **Primary color** and **Accent color** are overwridden by the custom styles, so you can ignore those.
* The default **Font** can be changed here if necessary, although Roboto is required for this project.
* Click **Publish** in the top right corner.
* You should see a green bar saying it was published.
* Click the **View Portal** link in the top right corner to see your newly themed Portal.
* And you're done!

## The Favicon File

Upload the included `favicon.ico` to the Logo options section on the Themes page.

## The Assets Folder

This folder contains the images that must be uploaded to the Assets section of the portal. If a file already exists, delete it first, then upload the file from this folder.

### Logo files

The `assets/idp-logo.png` file should be used to replace the existing asset file of the same name. This changes the logo shown on the login page for the portal. Update this file as needed, but commit the new file back here so it can easily found if someone needs it.

* In the Portal admin section (in Apigee), go to the **Assets** section.
* Find the `idp-logo.png` file and delete it.
* Click the **+File** button in the top left and add the logo file you want to use. It **MUST** be named idp-logo.png.
* Check your login page to see the new logo file.
* Use the `logo.png` file (it's likely to be the same as `idp-logo.png`) for the Primary logo and Mobile logo (see Themes page options on the right and the Logo options drop-down below that).

## The Pages Folder

All the pages for the site are saved here as Markdown files. They may not actually display properly when viewed in e.g. Bitbucket, due to included HTML and some frontmatter included between the --- lines.

Simply copy and paste the content below the frontmatter into new Pages in the portal, and use the values in the frontmatter for the Title, Path and Description fields.

## The Menus Folder

Check the two files for the header and footer menu links. Use the information shown to create them in the Menus section of the portal. To create a sub-menu (drop-down), create the menu item below the parent item, then click the second icon to the right of the menu item (Nest this...). The menu item should have an indent arrow in front of it when this is done. Click that icon again to remove the nesting.

## The APIs Folder

This contains the OpenAPI 3 specification yaml files for each API we have. When adding an API to the Portal you can use these files, or the specs already loaded into Apigee, just check which one is the latest.
