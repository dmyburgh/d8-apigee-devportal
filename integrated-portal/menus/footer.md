# Footer menu

Below is the menu structure for the **Footer** menu. On the Menus page for the portal, there is a drop-down in the top left that allows you to switch between header and footer menus.

* Home: /index
* API Products: /api-products
* Terms and Conditions: /terms
