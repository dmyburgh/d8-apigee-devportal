# Header menu

Below is the menu structure for the **Header** menu. On the Menus page for the portal, there is a drop-down in the top left that allows you to switch between header and footer menus.

* Get Started: /start
* API Overview: /api-overview
* API Products: /api-products
* FAQs: /faq
* Login: /login
