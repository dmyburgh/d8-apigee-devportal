/**
 * @file
 * Custom styling for the CN dev portal.
 * 
 * The styles are structured as follows (to get to each section,
 * just search for the section name with Ctl-F):
 *  - General styles
 *  - New classes
 *  - Main page layout styles
 *  - Typography overrides
 *     - Links
 *  - Nav header overrides
 *  - Title/breadcrumb bar styles
 *  - Form overrides
 *  - Button overrides
 *  - Card overrides
 *  - Footer overrides
 *  - New Custom components
 *     - Promo bar (chevron bar)
 *     - Full width panel
 *     - Register now CTA (currently unused)
 *     - Horizontal flow chart
 *     - Flexbox blocks
 *     - Two column CSS Grid layout
 *  - Page-specific overrides
 *     - Home page
 *     - APIs page (/apis)
 *     - Start page (/start)
 *     - My Apps page (/my-apps)
 */

// ---------------------------------------------------------------------
// General styles
// ---------------------------------------------------------------------
body {
  background: #fff;
}

pre {
  overflow-x: auto;
}

dt {
  font-weight: bold;
  font-size: 1.1rem;
}

dd {
  margin: 0.5rem 1rem 1.5rem;
}

audio,
video,
iframe {
  max-width: 100%;
}

// ---------------------------------------------------------------------
// New classes
// ---------------------------------------------------------------------

img.responsive {
  width: 100%;
}

mat-icon.icon-large {
  font-size: 36px;
  width: 36px;
  height: 36px;
}

.bg-circle {
  background-color: map-get($cn-grey, 350);
  border-radius: 50%;
  padding: 8px;
}

.flex-align-center {
  display: flex;
  align-items: center;
}

$sides: top, right, bottom, left;
@each $side in $sides {
  .margin-#{$side} {
    margin-#{$side}: 1rem !important;
  }
  .margin-#{$side}-2x {
    margin-#{$side}: 2rem !important;
  }
  .margin-#{$side}-3x {
    margin-#{$side}: 3rem !important;
  }
  .padding-#{$side} {
    padding-#{$side}: 1rem !important;
  }
  .padding-#{$side}-2x {
    padding-#{$side}: 2rem !important;
  }
  .padding-#{$side}-3x {
    padding-#{$side}: 3rem !important;
  }
}

.list-unstyled {
  margin: 0;
  padding: 0;
  list-style: none;
}

// ---------------------------------------------------------------------
// Main page layout.
// ---------------------------------------------------------------------
// Make the page less wide on bigger screens. Make sure you don't assign
// left-right padding to elements that use these classes.
.nav-header .mat-toolbar,
.context-bar.mat-toolbar,
.main .main-content,
.mat-toolbar-row,
.mat-toolbar-single-row,
.full-width {
  padding-left: $page-padding-sm;
  padding-right: $page-padding-sm;

  @media (min-width: 1200px) {
    padding-left: $page-padding-md;
    padding-right: $page-padding-md;
  }

  @media (min-width: 1500px) {
    padding-left: $page-padding-lg;
    padding-right: $page-padding-lg;
  }
}

// ---------------------------------------------------------------------
// Typography overrides.
// ---------------------------------------------------------------------
h1, h2, h3, h4, h5, h6 {
  color: map-get($cn-secondary, 300);
}

h1,
.context-bar h1,
.main-content h1 {
  font-size: 2.3rem;
  font-weight: normal;
}

h2,
.main-content h2 {
  font-size: 1.85rem;
  margin-top: 2rem;
}

.ng-star-inserted > h2:first-child {
  margin-top: 0;
}

h3,
.main-content h3 {
  font-size: 1.5rem;
  margin-top: 1.5rem;
}

h4,
.main-content h4 {
  font-size: 1.25rem;
}

h5,
.main-content h5 {
  font-size: 1rem;
}

h6,
.main-content h6 {
  font-size: 0.875rem;
}

// Links
// ======================================
body a:not([class*="mat-"]),
body a:not([class*="mat-"]):visited {
  color: map-get($cn-primary, 600);
}

body a:not([class*="mat-"]):hover,
body a:not([class*="mat-"]):hover:visited {
  color: map-get($cn-primary, 900);
}

// ---------------------------------------------------------------------
// Nav Header overrides.
// ---------------------------------------------------------------------
.nav-header {
  .mat-toolbar {
    &.mat-toolbar-row,
    &.mat-toolbar-single-row {
      height: $navbar-height;
      min-height: $navbar-height;
      justify-content: flex-start;
    }

    // Make the menu items align left.
    .nav-header-spacer.left {
      flex-grow: initial;
    }
    
    .menu-item:hover {
      color: map-get($cn-primary, 300);
      border-radius: 0;
      border-bottom: 2px solid map-get($cn-primary, 300);
      
      // Remove light grey background on focus.
      .mat-button-focus-overlay {
        opacity: 0;
      }
    }

    .site-logo {
      max-height: 20px;

      &.normal {
        margin-right: 50px;
      }
    
      &.mobile {
        margin-left: 60px;
      }
    }

    .mat-button {
      font-weight: normal;
      height: $navbar-height;
      line-height: $navbar-height;
    }
    
    // Logged in user menu item.
    .nested-menu .parent-menu-item.mat-button {
      line-height: $navbar-height;
    }
  }
}

// Mobile menu.
.responsive-menu-header .responsive-menu-item-container .responsive-menu-item {
  text-align: left;
  padding-left: 50px; // Allows space for drop-down icon.
  background-color: map-get($cn-grey, 400);
}

// ---------------------------------------------------------------------
// Title/Breadcrumbs Bar overrides.
// ---------------------------------------------------------------------
.context-bar {
  &.mat-toolbar {
    height: 80px;
    top: $navbar-height;
    background: map-get($cn-grey, 300);
    border-bottom: 1px solid map-get($cn-grey, 400);
  }
}

// ---------------------------------------------------------------------
// Form overrides.
// ---------------------------------------------------------------------
// Underlines disappear when you click into the field due to primary-color
// being white. Force the focused ripple to use a visible color.
.mat-form-field.mat-focused {
  .mat-form-field-label {
    color: map-get($cn-grey, 700);
  }
  .mat-form-field-ripple {
    background-color: map-get($cn-grey, 700);
  }
}

.mat-input-element {
  caret-color: map-get($cn-grey, 700);
}

// ---------------------------------------------------------------------
// Button overrides.
// ---------------------------------------------------------------------
// Remove rounded corners.
.mat-button,
.mat-raised-button {
  border-radius: 0;
}

// Changed raised button styles, and make login link look the same.
.mat-raised-button,
#nav-header-login-link-login {
  background-color: map-get($cn-primary, 300);
  color: #fff;

  &:hover {
    background-color: map-get($cn-primary, 600);
    border: none;
  }
}

// Give login button some extra space.
#nav-header-login-link-login {
  margin-left: 50px;
}

.mat-button.mat-primary,
.mat-icon-button.mat-primary,
.mat-stroked-button.mat-primary,
.mat-input-element {
  color: map-get($cn-grey, 700);
  border-radius: 0;
}

// ---------------------------------------------------------------------
// Card overrides.
// ---------------------------------------------------------------------
.mat-card {
  background: map-get($cn-grey, 300);
}

// ---------------------------------------------------------------------
// Footer overrides.
// ---------------------------------------------------------------------
.nav-footer {
  display: block;

  .mat-toolbar {
    &.mat-primary {
  	  background: map-get($cn-grey, 700);
    }

    .mat-button {
      color: map-get($cn-grey, 400);

      &:hover {
        color: map-get($cn-grey, 500);
      }
    }
  }
}
// ---------------------------------------------------------------------
// Custom Components
// ---------------------------------------------------------------------

// Custom component: Promo bar
// ======================================
.cn100-bar {
  background: url(/files/chevrons.png) repeat-x;
  margin-top: $default-vertical-space;

  p {
    display: flex;
    justify-content: center;
    align-items: center;
    margin: 0;
    padding: 20px 0;
    background: map-get($cn-grey, 350);

    @media (min-width: 768px) {
      margin: 0 20%;
    }

    @media (min-width: 960px) {
      margin: 0 30%;
    }
  }

  img {
    padding-right: 10px;
  }
}

// Custom component: Full width panel
// =====================================
.panel {
  text-align: left;
  background: map-get($cn-grey, 100);
  // Only assign top and bottom here to not mess with left/right padding
  // on the .full-width class on this same div.
  padding-top: $default-vertical-space;
  padding-bottom: $default-vertical-space;

  @media (min-width: 768px) {
    display: flex;
    
    &.text-image-right {
      .panel-image {
        margin-left: 50px;
      }
    }

    &.text-image-left {
      .panel-image {
        margin-right: 50px;
      }
    }
  }
  
  .panel-text {
    & > h2 {
      margin-top: 0;
    }
  }
  
  .panel-image {
    img {
      width: 100%;
    }
  }
}

// Custom component: Register now CTA
// =====================================
.register-now-cta {
  text-align: left;
  margin-top: $default-vertical-space;

  @media (min-width: 768px) {
    display: flex;
    justify-content: space-between;
    align-items: center;
  }

  h2 {
    margin-top: 0;
  }
}

// Custom component: Horizontal Flow Chart (List)
// ==============================================
.flow-chart-horizontal {
  padding: 0;
  margin: 0;
  list-style: none;
  display: flex;

  li {
    padding: 1rem;
    border: 1px solid map-get($cn-grey, 500);
    background: map-get($cn-grey, 400);
    margin: 0;
    
    &.separator {
      color: map-get($cn-grey, 500);
      border: 0;
      background: none;
      font-size: 40px;
    }
  }
}

// Custom component: Flexbox blocks e.g. /api-products
/*
<div class="flex-wrapper">
	<div class="item">
		<h3>Heading <mat-icon>icon_name</mat-icon></h3>
		<p class="item-content">Lorem ipsum</p>
		<a href="/location-api" class="item-link">Read more</a>
	</div>
	...
</div>
*/
// ======================================
.flex-wrapper {
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;

  .item {
    flex-basis: 30%;
    min-width: 285px;
    margin: 0 3% 3rem 0;

    mat-icon {
      font-size: 36px;
      width: 36px;
      height: 36px;
      margin-left: 10px;
    }
    
    h3 {
      font-size: 1.3rem;
      font-weight: bold;
      margin: 1rem 0 0.5rem;
      display: flex;
      align-items: center;
    }
    
    .item-link {
      font-weight: bold;
      text-transform: uppercase;
    }
  }
  
  // Add this class to the wrapper element to get two child elements to be
  // left and right justified on the same line.
  &.space-between {
    align-items: center;
    justify-content: space-between;
  }
}

// Custom component: Two column grid layout
// ======================================
.grid-right-aside {
  display: grid;
  grid-template-columns: auto 20vw;
  grid-gap: 2rem;
}

// Custom component: Sticky Sidebar Nav e.g. on /api-overview page
// Required HTML:
// <main class="with-side-nav">
//   <nav>
//     <ul>
//       <li></li>
//       ...
//     </ul>
//   </nav>
//   <article>
//     ...rest of Markdown content...
//   </article>
// </main>
// ======================================
.with-side-nav {
  nav {
    ul {
      margin: 0;
      padding-left: 20px;
      columns: 2;

      &.level-two {
        columns: auto;
      }
    }

    li {
      margin: 0;
      padding-right: 1rem;
      
      a, a:visited {
        color: map-get($cn-grey, 800);
      }
    }
  }
 
  @media screen and (min-width: 980px) {
    display: grid;
    grid-template-columns: 20vw auto;

    nav {
	  @media screen and (min-height: 560px) {
        position: sticky;
        top: 50px;
        height: 100vh;
      }

      ul {
        columns: auto;
      }
    }
    
    // Remove margin of first H2 so it lines up better with side menu.
    h2:first-of-type {
    	margin-top: 0;
	}
  }
}

// ---------------------------------------------------------------------
// Page-specific Overrides.
// ---------------------------------------------------------------------

// Home Page overrides (#index is front)
// ======================================

// ---------------------------------------------------------------------
// WARNING: Be VERY careful when targeting page level CSS ids. They
// change according to the page title, so if you change the title, your
// styling won't apply until you update the CSS id here as well.
// ---------------------------------------------------------------------

// Hide default home page body background image...
#index {
  background-color: map-get($cn-grey, 300);

  &.main {
    height: calc(100% - $navbar-height);
    margin-top: $navbar-height;

    .main-content {
      background: none;

      .home-page-card {
        &:hover {
          box-shadow: 0 2px 8px 0 rgba(0, 0, 0, 0.3);

          .home-page-card-actions {
            background: map-get($cn-primary, 300);
            color: map-get($cn-grey, 100);
          }
        }
      }

      .home-page-card-content {
        background: map-get($cn-grey, 100);
        border-bottom: 1px solid rgba(0, 0, 0, 0.125) !important;
      }

      .home-page-card-actions {
        background: map-get($cn-grey, 100);
        color: map-get($cn-primary, 600);
      }
    }
  }
}

// ...and only apply an image to the home page CTA.
#index {
  &.main {
    .main-content {
      @media (min-width: 768px) {
        min-height: calc(100% - 44px);
      }
    }

    .home-page-banner {
      background-image: url(/files/2019-05-06-CN100-video-banner-1680.jpg);
      background-size: cover;
      background-position-y: center;
      height: 100%;
      margin-bottom: $default-vertical-space;
    }

    .home-page-cta {
      // Enable this next line if you need a dark overlay on the image added above.
      // background: rgba(0, 0, 0, 0.55);
      color: #fff;
      height: 380px;
      display: flex;
      flex-direction: column;
      justify-content: center;
      padding: 0 $page-padding-sm;
      text-align: left;

      @media (min-width: 1200px) {
        padding-left: $page-padding-md;
      }

      @media (min-width: 1500px) {
        padding-left: $page-padding-lg;
      }

      h1.home-page-cta-title {
        color: #fff;
        font-weight: 700;
        font-size: 2.125rem;
        line-height: 2.125rem;
      }

      p.home-page-cta-subtitle {
        font-size: 1.2rem;
      }
    }
  }
}

// API Page Overrides.
// ======================================
#apis {
  // Hide search function for now.
  .api-doc-filter {
    display: none;
  }
}

// Override fixed width to avoid cut off text.
@media (min-width: 601px) {
  .container[_ngcontent-c5] .pergamon-content[_ngcontent-c5] {
    width: auto;
  }
}

// Start Page Overrides
// ======================================
#start {
  .main-content {
    img {
      border: 1px solid #eee;
    }
  }
}

// My Apps Overrides
// ======================================
#my-apps {
  .main-content {
    background-image: url(/files/2019-03-26-AGM-1680.jpg);
    background-size: cover;
    background-position-y: left;
  }
  // Because we have the background image above, we need the content to have a white background.
  // You can remove this if you remove the background image.
  .app-card,
  form .app-buttons,
  .apps-list-empty {
    background: #fff;
  }
  .apps-list-empty {
    padding-bottom: 30px;
  }
  
  @media (max-width: 767px) {
    .app-card {
      .app-buttons {
        position: initial;
      }

      .app-layout-section {
        display: block;
        min-width: initial;
    
        .app-layout-section-content {
          min-width: initial;
          width: 100%;

          .mat-form-field {
            width: auto;
          }
        }

        // Make the API keys table responsive.
        .app-api-keys {
          overflow-x: auto;

          table {
            min-width: 500px;
          }
        }
      }
    }
  }
}
