---
name: Home
path: index
description: Home
---
<div class="home-page">

	<div class="home-page-banner">
    <div class="home-page-cta">
      <h1 class="home-page-cta-title">
        CN API Portal
      </h1>
      <p class="home-page-cta-subtitle">
        Use our diverse APIs to create innovative apps for transforming your business.
      </p>
      <a mat-raised-button
         id="home-page-cta-get-started"
         href="/start">
        Get started
      </a>
    </div>
	</div>

  <div class="home-page-cards">

    <mat-card class="home-page-card quick-start"
              href="/start">
      <mat-card-header class="home-page-card-header"
                       color="primary">
        <mat-icon class="home-page-card-header-icon">
          check_circle
        </mat-icon>
        <h2 class="home-page-card-header-text">
          Quick Start
        </h2>
      </mat-card-header>
      <mat-card-content class="home-page-card-content">
        <p class="home-page-card-content-text">
          Get up and running with our APIs in no time at all.
        </p>
      </mat-card-content>
      <mat-card-actions class="home-page-card-actions">
        <button mat-button
                class="home-page-card-action">
          Begin
        </button>
      </mat-card-actions>
    </mat-card>

    <mat-card class="home-page-card view-apis"
              href="/api-products">
      <mat-card-header class="home-page-card-header"
                       color="primary">
        <mat-icon class="home-page-card-header-icon">
          class
        </mat-icon>
        <h2 class="home-page-card-header-text">
          APIs
        </h2>
      </mat-card-header>
      <mat-card-content class="home-page-card-content">
        <p class="home-page-card-content-text">
          Learn all about our APIs and give them a test run.
        </p>
      </mat-card-content>
      <mat-card-actions class="home-page-card-actions">
        <button mat-button
                class="home-page-card-action">
          View APIs
        </button>
      </mat-card-actions>
    </mat-card>

  </div>

  <div class="panel full-width text-image-right">
    <div class="panel-text">

## Added Value for Your Customers

Customers expect today's businesses to provide relevant and connected experiences across multiple channels.

Partner with us to build a seamless customer experience with speed, scalability, and security.

Experience the agility that comes from ready-to-use APIs built to power enterprise-class commerce.

Leverage the power of our developer community to get your products to the starting line faster.

		</div>

    <div class="panel-image">

![Train](/files/train.jpg)

    </div>
  </div>

  <div class="cn100-bar">

![Celebrating 100 years](/files/100CC.png) Celebrating 100 years. [#CC100](https://www.cc100.ca/en/)

  </div>

</div>
