---
name: FAQs
path: faq
description: Frequently Asked Questions
---
<dl>
    <h2>Getting Started</h2>

    <dt>Do I need to register?</dt>
    <dd>You are now required to register for and use a key to access the data in the APIs. Nevertheless, you may still use the APIs and other data free of charge and you are allowed unlimited access. Just don’t spike us ;-)</dd>

    <dt>Are there any data limits for a single application or a developer?</dt>
    <dd>Currently there are no limits, however, we track traffic associated with an individual API key.</dd>

    <dt>How come I’m getting “error”: “Unauthorized” when I make a call to the API?</dt>
    <dd>You are now required to register for and use a key to access the data in the APIs.</dd>

    <dt>What kind of data can I get from the API?</dt>
    <dd>Visit the links on the Home page to learn more about the different data sets and their methods.</dd>

    <h2>Monetization</h2>

    <dt>Does ITA charge for this service?
    <dd>Yes. It’s not free.</dd>

    <dt>Does ITA allow for profit applications that use this data?</dt>
    <dd>Yes, any application (including those for-profit) are welcome to use the API in any way that they see fit.</dd>

    <h2>Data Formats</h2>

    <dt>What is the format of this feed?</dt>
    <dd>API data is accessible as JavaScript Object Notation (JSON).</dd>

    <dt>Do you also provide XML for these data sets?</dt>
    <dd>No. We are not planning to publish XML for the data at this time.</dd>
</dl>