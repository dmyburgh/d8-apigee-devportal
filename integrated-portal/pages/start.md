---
name: Quick Start
path: start
description: Get started using APIs
---
Get started using the APIs in three steps:

<nav class="page-navigation">
    <ul class="flow-chart-horizontal">
        <li><a href="/quick-start#sign-in">1. Sign in</a></li>
        <li class="separator" aria-hidden="true">&rarr;</li>
        <li><a href="/quick-start#register-apps">2. Register apps</a></li>
        <li class="separator" aria-hidden="true">&rarr;</li>
        <li><a href="/quick-start#access-keys">3. Access the API keys</a></li>
    </ul>
</nav>

<a name="sign-in"></a>
## 1. Sign in

To sign in to the portal:

* Click **Login**.  
![Click the login link](/files/portal-login.png)  
* Enter your email address and password.  
![The portal login page](/files/portal-login-screen.png)  
* Click **Sign In**.

<a name="register-apps"></a>
## 2. Register apps

To register an app:

* Select **Apps** from the user drop-down.  
![Click the username to get the drop-down](/files/portal-profile-dropdown.png)  
* Click **+ New App** to create a new app.  
![The New App button](/files/portal-add-app.png)  
* Enter a name and description for the app in the New App dialog, and select the API Product(s) you want to use.  
![Adding a new app](/files/portal-new-app-page.png)  
* Click **Create**.

<a name="access-keys"></a>
## 3. Access the API keys

* Click the name of the app on the My Apps page.
* The API keys are shown in the middle of the page.  
![Getting the API keys for the app](/files/portal-app-keys.png)  
* Click the **Show** link to see the Secret key.

For a more detailed process read our [Documentation](/documentation).
