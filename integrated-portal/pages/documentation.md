---
name: Documentation
path: documentation
description: Documentation on how to use the APIs
---
<main class="with-side-nav">
<nav role="side-navigation" class="side-nav">
    <ul>
        <li><a href="/documentation#intro">Introduction</a></li>
        <li><a href="/documentation#prerequisite">Prerequisite</a></li>
        <li><a href="/documentation#auth">Authentication and Authorization</a></li>
        <li><a href="/documentation#guides">How to Guides</a>
            <ul class="level-two">
                <li><a href="/documentation#register">Register Apps</a></li>
                <li><a href="/documentation#generate">Generate Token</a></li>
                <li><a href="/documentation#refresh">Refresh Token</a></li>
                <li><a href="/documentation#access">Access Resource</a></li>
            </ul>
        </li>
        <li><a href="/api-specifications">API List</a></li>
        <li><a href="/documentation#versioning">Versioning</a></li>
        <li><a href="/documentation#limiting">Rate Limiting</a></li>
        <li><a href="/documentation#errors">Errors</a></li>
        <li><a href="/documentation#glossary">Glossary</a></li>
        <li><a href="/faq">FAQs</a></li>
        <li><a href="documentation#support">Support</a></li>
    </ul>
</nav>

<article>

<a id="intro"></a>
## Introduction

CN's developer platform offers APIs and resources that enable partners to access the information and details of the shipments at various stages of supply chain.

In following section, we will describe the process to register and access the API's.

<a id="prerequisite"></a>
## Prerequisite

* Click on the login page and submit a request to create an account.
* Receive the login Credentials from CN.
* Visit the developer portal with an approved account to create a CN app and generate your authentication tokens. These include 'consumer' tokens and secrets for app authentication. For more information on authentication click here (getting started page link)

<a id="auth"></a>
## Authentication and Authorization

CN will be following the OAuth–Client credentials framework to facilitate the authentication and authorization process. Before calling the Location API, partners should first send the request to OAuth endpoint to generate the Token and refresh token. After receiving the token, partners should send a request to the resource API with the token to get the response. Please refer to below token process.

**Endpoint:** https://api-stg-ext.cn.ca/oauth/token/accesstoken?grant_type=credentials

**Sample Request**
```
POST /v1/oauth/token HTTP/1.1
Host: api-stg-ext.cn.ca:10443
Authorization: Basic czZCaGRSa3F0MzpnWDFmQmF0M2JW
Content-Type: application/x-www-form-urlencoded

grant_type=client_credential
```

**Sample Response**
```
Success:
HTTP/1.1 200 OK
Content-Type: application/json;charset=UTF-8
Cache-Control: no-store
Pragma: no-cache

{
"refresh_token_expires_in":"0",
"api_product_list":"[Shipments-Product, ShipmentStatus-Product]",
"api_product_list_json":[
"Shipments-Product",
"ShipmentStatus-Product"
],
"organization_name":"cnapi",
"developer.email":"anshu.ranjan@cn.ca",
"token_type":"BearerToken",
"issued_at":"1562073508279",
"client_id":"cPrzccrbnzGGuG8MuPtzjQvjW4UqsT4A",
"access_token":"cCYAffApRMB6Et19gGqQoDVZAn3F",
"application_name":"2a4b8d02-c304-47f4-a205-5d75f483f423",
"scope":"",
"expires_in":"3600",
"refresh_count":"0",
"status":"approved"
}
```

<a id="refresh"></a>
### Refresh Token

If the token is required for more than 30 minutes, Partners should send a request to refresh the token. CN authorization server validates the refresh token request and issues a new Access token. Please refer to the sample request and endpoint below.

**Endpoint:**
https://api-stg-ext.cn.ca/oauth/token/refresh_accesstoken?grant_type=refresh_token&refresh_token=IBWQphLC3GphbxSH2JBTOgNkglxZ


**Request**
```
POST /oauth/token/refresh_accesstoken HTTP/1.1
Host: api-stg-ext.cn.ca
Authorization: Basic czZCaGRSa3F0MzpnWDFmQmF0M2JW
Content-Type: application/x-www-form-urlencoded

grant_type=refresh_token&refresh_token=tGzv3JOkF0XG5Qx2TlKWIA
&client_id=s6BhdRkqt3&client_secret=7Fjfp0ZBr1KtDRbnfVdmIw
```

**Success Response**
```
HTTP/1.1 200 OK
Content-Type: application/json;charset=UTF-8
Cache-Control: no-store
Pragma: no-cache

{
"refresh_token_expires_in":"1800000",
"api_product_list":"[Shipments-Product, ShipmentStatus-Product]",
"api_product_list_json":[
"Shipments-Product",
"ShipmentStatus-Product"
],
"organization_name":"cnapi",
"developer.email":"anshu.ranjan@cn.ca",
"token_type":"BearerToken",
"issued_at":"1562073508279",
"client_id":"cPrzccrbnzGGuG8MuPtzjQvjW4UqsT4A",
"access_token":"cCYAffApRMB6Et19gGqQoDVZAn3F",
"application_name":"2a4b8d02-c304-47f4-a205-5d75f483f423",
"scope":"",
"expires_in":"1800000",
"refresh_count":"1",
"status":"approved"
}
```

<a id="guides"></a>
## How to Guides

<a id="register"></a>
### How to Register Apps and get the Client id and secret
Please refer to [getting started](/start) page.

<a id="generate"></a>
### How to Generate Token
Refer to [Authentication](/documentation#auth) section.

### How to Refresh Token
Refer to [Refresh](/documentation#refresh) token section.

<a id="access"></a>
### How to Access Resource

<a id="versioning"></a>
## Versioning
Please refer to [API specification](/api-specifications) page

<a id="limiting"></a>
## Rate Limiting
All the authenticated request, user can access the resource once in every 15 minutes.

<a id="errors"></a>
## Errors

<a id="glossary"></a>
## Glossary

<a id="support"></a>
## Support
Please contact XXX.XXX@cn.ca for any questions or support.

</article>
</main>
