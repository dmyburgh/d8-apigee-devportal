---
name: GPS Location Tracking
path: location-api
description: Location API Info page
---
<p class="margin-bottom-2x"><img class="responsive" src="/files/cn-intermodal-lift.jpg" /></p>

<div class="grid-right-aside">
<main>

CN is the first class 1 railroad to offer precise and real-time tracking capabilities via a modern and secured API platform. This solution allows CN customers to monitor the movement of shipments and retrieve the last GPS position either on the rail network or using CNTL trucking services.

Having real time visibility of shipments enable better planning and resource allocation to manage a facility or equipment assets. A unique feature of the GPS Location API is that customers are benefiting from advanced GPS technology utilized to monitor rail and trucking operations that is now made available to trace shipments.

Customers can either track a specific equipment, vehicle identification number or internal reference number and the API will retrieve the latest GPS coordinates.

This solution enables customers to easily integrate this critical information to their ecosystem at their own pace and at a reasonable costs compared to other integration options.

<p>&nbsp;</p>
<p><a href="/docs/locationapi-product/1/overview" mat-raised-button>See the API Spec</a></p>
</main>

<aside>

**One stop shop for tracking Rail, Track and Station**

<ul class="list-unstyled margin-top-2x">
  <li class="flex-align-center margin-bottom-2x"><mat-icon class="icon-large margin-right bg-circle">map</mat-icon>Precise GPS location of your shipment</li>
  <li class="flex-align-center margin-bottom-2x"><mat-icon class="icon-large margin-right bg-circle">tap_and_play</mat-icon>On Demand and Real Time Visibility</li>
  <li class="flex-align-center margin-bottom-2x"><mat-icon class="icon-large margin-right bg-circle">category</mat-icon>Simple System integration and testing</li>
</ul>

</aside>
</div>
