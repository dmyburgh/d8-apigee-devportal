---
name: Get Started
path: get-started
description: Everything you need to know to get started
---
## Introduction
Our APIs offer the following benefits:

* **Get started right away**: Visit our Developer Portal to find out what our APIs do, how they work and what you need to do to interface with them. Use our sandboxes and get building now. Our APIs need just a few lines of code to run in your applications.
* **Clear overview**: You always know what is happening. Use our API analytics to track your users’ activity in your applications and adjust your tactics if necessary.
* **Be creative**: We are giving you the opportunity to use exclusive APIs to help shape the future of banking.

The CC API developer portal contains a wealth of information about our business ready APIs. Take some time familiarizing yourself with the portal before starting to build your apps.

[Step 1: How to Use](start#how-to-use)  
[Step 2: How to Go Live](start#how-to-go-live)  
[Step 3: Authentication](start#authentication)  
[Step 4: Error Handling](start#error-handling)  
[Step 5: Support](start#support)

<a name="how-to-use"></a>
## How to Use
### Registration
To use our API products, just create a Developer account.

Creating an account is quick and easy:

* Click on **Sign In**.
* Fill in the form with your personal details, including your name, email address and username.
* Submit the form.
* You will receive an email with an activation link. Click the link, and you are ready to go!

### Explore API Products
Want to be inspired and learn what you could build? Our available APIs are displayed on the API Products page. From here you can navigate to the underlying API Overview pages for details of a specific API. Next, go to the Documentation pages for technical information.

<a name="how-to-go-live"></a>
## How to Go Live
Once you have tested your application and related proposition(s) in our sandbox environment, you can submit a request to go live.

* First, create a new App for production. By doing so, you will get a different API-Key which you can use for your production environment.
* To get production access, it is important that both your account and App details are registered correctly.
* Subscribe to the API products you want to use in the production environment.
* Submit your request and contact us to request production access. Depending on the API, our API Services team might contact you to discuss the details of your request.
* Once approved, you are ready to use our API in a live environment.

<a name="authentication"></a>
## Authentication
The authentication for this API is done on the basis of access token. To obtain an access token, you need to call our OAuth API. The authentication mechanism can vary per API depending on the required level of authentication.

### Environments
These are the URLs for the endpoints to get the tokens.

Sandbox: https://auth-sandbox.connect.example.com

Production: https://auth.connect.example.com

### OAuth Flows
Our APIs are secured using OAuth 2.0. This means you will need to pass an access token when making your request to the APIs. This page will explain how to obtain an access token. We use the following three flows for getting an access token: Client credentials, Authorization code flow and Refresh token.

If you want to go more in depth on some of the OAuth protocol, have a look at the OAuth RFC.

<a name="error-handling"></a>
## Error Handling
This section provides an overview of the CC APIs error model.

### Error Format
All CC APIs throw errors in following JSON format:

```
{
  "errors": [
    {
      "code": "ERROR_UNIQUE_CODE",
      "message": "ERROR_MESSAGE",
      "category" : "SHORT_DESCRIPTION/ERROR_CATEGORY",
      "reference" : "API_DOCUMENTATION_LINK",
      "status" : "HTTP_STATUS_CODE",
      "traceId" : "UUID"
    }
  ]
}
```

<a name="support"></a>
## Support
Questions or problems? We are happy to help. Please check our [FAQ](/faq) page for the answers to frequently asked questions.

Can’t find the answer you need? Please contact us and we will get back to you as soon as possible.
