---
name: API Products
path: api-products
description: Landing page to list out the API products and their APIs
---
<img class="responsive" src="/files/cn-intermodal-lift.jpg" />

## Track + Trace
<hr />
<div class="flex-wrapper">
  <div class="item">
    <h3>
      GPS Location Tracking
      <mat-icon>
        place
      </mat-icon>
    </h3>
    <p class="item-content">
      Monitor the movement of your shipments and retrieve the last GPS position either on the rail network or using CNTL tracking services.
    </p>
    <p class="flex-wrapper space-between">
      <a href="/location-api" class="item-link">Read more</a>
      <button class="mat-raised-button" href="/docs/locationapi-product/1/overview">API Spec</button>
    </p>
  </div>

  <div class="item">
    <h3>
      Shipment Status
      <mat-icon>
        compass_calibration
      </mat-icon>
    </h3>
    <p class="item-content">
      Get timely reported events for your shipment. The format provides flexibility to integrate with a tracking system that can complement or replace events reported via EDI 214 or CLM.
    </p>
    <p class="flex-wrapper space-between">
      <a href="/shipment-api" class="item-link">Read more</a>
      <button class="mat-raised-button" href="/docs/locationapi-product/1/overview">API Spec</button>
    </p>

  </div>
  
  <div class="item">
    <h3>
      Bill of Lading Information
      <mat-icon>
        list_alt
      </mat-icon>
    </h3>
    <p class="item-content">
      Retrieve Bill of Lading information including parties, origin/destination, commodity, shipper reference numbers.
    </p>
    <p class="flex-wrapper space-between">
      <a href="/bill-of-lading-api" class="item-link">Read more</a>
      <button class="mat-raised-button" href="/docs/locationapi-product/1/overview">API Spec</button>
    </p>

  </div>
  
  <div class="item">
    <h3>
      Empty Equipment
      <mat-icon>
        vibration
      </mat-icon>
    </h3>
    <p class="item-content">
      Get the status of the empty equipment request, equipment specifications & ID and ETA for expcted delivery.
    </p>
    <p class="flex-wrapper space-between">
      <a href="/empty-equipment-api" class="item-link">Read more</a>
      <button class="mat-raised-button" href="/docs/locationapi-product/1/overview">API Spec</button>
    </p>

  </div>
  
  <div class="item">
    <h3>
      Estimated Time of Arrival
      <mat-icon>
        timer
      </mat-icon>
    </h3>
    <p class="item-content">
      Get the estimated time of arrival of loaded equipment on rail network at either an interchange point, rail destination or customer industry. For door services, the appointment management API should be used in combination with rail network ETA.
    </p>
    <p class="flex-wrapper space-between">
      <a href="/eta-api" class="item-link">Read more</a>
      <button class="mat-raised-button" href="/docs/locationapi-product/1/overview">API Spec</button>
    </p>

  </div>
</div>
